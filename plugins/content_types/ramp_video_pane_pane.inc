<?php
/**
 * This plugin array is more or less self documenting
 */
$plugin = array(
  'title' => t('RAMP Video pane'),
  'single' => TRUE,
  // oh joy, I get my own section of panel panes
  'category' => array(t('RAMP Video'), -9),
  'edit form' => 'rampvideopane_pane_content_type_edit_form',
  'render callback' => 'rampvideopane_pane_content_type_render',
);


/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */
function rampvideopane_pane_content_type_render($subtype, $conf, $context = NULL) {
  // Build the content type block.
  $block = new stdClass();
  $block->module  = 'ramp_video_pane';
  
  if (isset($conf['override_title'])) {
    $block->title = $conf['override_title'];
  }
  else {
    $block->title = NULL;
  }
  
  $width = $conf['ramp_video_width'];
  $height = round($width * 0.6125);
  
  $iframe_height = $height;
  // add extra height for video options that appear below the video
  if(isset($conf['ramp_video_options']['title']) || isset($conf['ramp_video_options']['transcript']) || isset($conf['ramp_video_options']['description'])) {
    $iframe_height += 10; // top margin on area below video - div class "ez-media-details"
    if(isset($conf['ramp_video_options']['title']) && $conf['ramp_video_options']['title']) {
      $iframe_height += 34;
    }
    if(isset($conf['ramp_video_options']['transcript']) && $conf['ramp_video_options']['transcript']) {
      $iframe_height += 132;
    }
    if(isset($conf['ramp_video_options']['description']) && $conf['ramp_video_options']['description']) {
      $iframe_height += 66; // might need extending if these aren't fixed length descriptions
    }
  }
  
  $domain = $_SERVER['HTTP_HOST']; // includes start which will break on local, dev and test
  $domain = explode('.', $domain);
  if(count($domain) > 0) {
    $domain[0] = 'www';
  }
  $domain = implode('.', $domain);
  
  $options = '';
  foreach(_rampvideopane_video_options() as $name => $description) {
    $options .= $name . '=';
    $options .= isset($conf['ramp_video_options'][$name]) && !$conf['ramp_video_options'][$name] ? 'false' : 'true';
    $options .= '&';
  }
  
  $output = '<div class="ramp-video-pane">';
  $output .= '<iframe src="http://' . $domain . '/videos/video-embed/' . $conf['ramp_video_id'] . '/video.htm?' . $options . 'width=' . $width . '&height=' . $height . '"
      height="' . $iframe_height . 'px" width="' . $width . 'px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" style="border:0; padding:0; margin:0;"></iframe>';
  $output .= '</div>';
  
  $block->content = $output;
  
  return $block;
}


/**
 * 'Edit form' callback for the content type.
 */
function rampvideopane_pane_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['ramp_video_id'] = array(
    '#type' => 'textfield',
    '#title' => t('RAMP Video ID'),
    '#size' => 50,
    '#description' => t('You can find this in the URL of the video page - it looks something like "55555555".'),
    '#default_value' => !empty($conf['ramp_video_id']) ? $conf['ramp_video_id'] : '',
    '#required' => TRUE,
  );
  
  $form['ramp_video_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#size' => 50,
    '#description' => t('Full width for landing pages is 609.  Height will be automatically calculated to prevent distortion.'),
    '#default_value' => !empty($conf['ramp_video_width']) ? $conf['ramp_video_width'] : '609',
    '#required' => TRUE,
  );
  
  $form['ramp_video_options'] = array(
    '#type' => 'checkboxes',
    '#title' => t('RAMP Embedded Video Options'),
    '#description' => t('Controls to customize the RAMP player.  Leave to defaults unless an advanced user.'),
    '#options' => _rampvideopane_video_options(),
    '#default_value' => !empty($conf['ramp_video_options']) ? $conf['ramp_video_options'] : array('title' => 'title'),
  );
  
  // no submit
  return $form;
}


/**
 * Submit function, note anything in the formstate[conf] automatically gets saved 
 */
function rampvideopane_pane_content_type_edit_form_submit(&$form, &$form_state) {
  $form_state['conf']['ramp_video_id'] = $form_state['values']['ramp_video_id'];
  $form_state['conf']['ramp_video_width'] = $form_state['values']['ramp_video_width'];
  $form_state['conf']['ramp_video_options'] = $form_state['values']['ramp_video_options'];
}

/**
 * Centralize video options since we use them for both functions 
 */
function _rampvideopane_video_options() {
  return array(
    'title' => t('Title'),
    'transcript' => t('Transcript'),
    'description' => t('Description'),
    //'ova' => t('Advertising'), - per "at this time this will default to false and should not be turned on"
    'bluedots' => t('Blue dots'),
    'overlay' => t('Overlay'),
  );
}